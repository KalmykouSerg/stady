﻿using System;
using System.Collections.Generic;

namespace Lesson3HomeWork
{
    public class CalculateClass
    {

        public static double[] StartCalulate(double creditValue, double percentValue, ushort moth)
        {
            double sumCredit;
            double sumPercent;
            double[] returnDictionary = new double[2];

            sumPercent = (creditValue * percentValue / 100);
            sumCredit = creditValue;

            returnDictionary[0] = sumPercent;
            returnDictionary[1] = sumCredit;

            return returnDictionary;
        }

    }
}
