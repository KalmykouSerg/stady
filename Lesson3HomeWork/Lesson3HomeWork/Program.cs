﻿using System;
//Homework:
//You should implement program - credit calculator for 1 year.

//The user enters the amount that he wants to take on credit, and percent at which he wants to take it.
//The program returns him a list of payments for year, and at the end the total amount (principal + percents).

//It is imperative to provide for validation of entered data:
//-The percentage mustn't be less than 0 and more than 100.
//- The amount mustn't be less than 1.
//- If you enter incorrect data, application shouldn't stop working.

//Template of interface:
//------------------------------------
//Enter credit amount:
//1000
//Enter credit percent:
//10
//Monthly Payments:
//1 month... rub
//2 month... rub...
//12 month... rub
//The total amount of payments will be: ... rub

namespace Lesson3HomeWork
{
    class Program
    {
        static void Main(string[] args)
        {
  
            const int month = 12;
            double creditValue;
            double percentValue;
            double paySumm;

            double[] totalSumm;
            string str;

            Console.WriteLine("Hello my dear friend, do you want calculate a credit ?");


            do // Check correct creditValue  1.... 10000
            {
                Console.WriteLine("Enter credit amount (1...1000) :");
                str = Console.ReadLine();
               
            } while (CheckRangeDoubleValue(1, 1000, str));

            creditValue = double.Parse(str);

            do // Check correct percentValue  0.... 100
            {
                Console.WriteLine("Enter percentage amount (0...100) :");
                str = Console.ReadLine();

            } while (CheckRangeDoubleValue(0, 100, str));

            percentValue = double.Parse(str);

            // Use Static Class for calculating
            totalSumm = CalculateClass.StartCalulate(creditValue, percentValue, month);
            paySumm = (totalSumm[0] + totalSumm[1]);

            Console.WriteLine("Monthly Payments:");
            for (int i = 0; i < month; i++)
            {
                Console.WriteLine($"{i+1} month: {paySumm/ month} rubn");
            }

            Console.WriteLine($"The total amount : {paySumm}");
            Console.WriteLine($"The total principal : {totalSumm[1]}");
            Console.WriteLine($"The total percents : {totalSumm[0]}");

        }

        static bool CheckRangeDoubleValue(double minValue, double maxValue, string currentValue)
        {
            double checkValue;
            bool wrongEnter = true;

            wrongEnter = !double.TryParse(currentValue, out checkValue);

            if (wrongEnter)
            {
                Console.WriteLine("Wrong entering data, please try again");

            } else
            {
                if (checkValue >= minValue & checkValue <= maxValue)
                {
                    Console.WriteLine("Correct entering");
                }
                else
                {
                    Console.WriteLine("Wrong entering range, please try again");
                    wrongEnter = true;
                }
            }
            return wrongEnter;
        }

    }
}
