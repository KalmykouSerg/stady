﻿using System;

namespace Lesson3_Operators
{
    class Program
    {
        static void Main(string[] args)
        {
            var type = typeof(String);



            // work 1
            short x = 9;
            short y = 5;

            if (x == y)
            {
                Console.WriteLine("x равен y");
            }
            else if (x != y)
            {
                Console.WriteLine("x не равно y");
            }
            else if (x < y)
            {
                Console.WriteLine("x меньше y");
            }
            else if (x > y)
            {
                Console.WriteLine("x больше y");
            }

            // work 2
            bool flag = true;
            int z = 5;

            Console.WriteLine(z += 3);
            Console.WriteLine(z -= 4);
            Console.WriteLine(z *= 3);
            Console.WriteLine(z /= 4);
            Console.WriteLine(z %= 3);


            // test
            Console.WriteLine(flag &= true);
            Console.WriteLine(flag |= true);
            Console.WriteLine(flag ^= true);

            // work 3


            int diam = 32;
            int radius = 32 / 2;

            double sque;
            double line;

            sque = 2 * Math.PI * radius;
            line = Math.PI * (radius * radius);

            int[] mass = new[] { 1, 2, 3, 4 };
            int value = mass[0];
            Console.WriteLine(type);

            int age = 18;

            if (age >= 18 & age > 50)
            {
                Console.WriteLine("True");
            }
            else
            {
                Console.WriteLine("False");
            }
            //checked() Вычисление в контролируемом контексте
            //unchecked() вычисление в неконтроллируемом контексте 

        }
    }
}

